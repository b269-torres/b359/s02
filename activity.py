
# Activity 1

while True:
    try:
        year = int(input("Enter a year: "))
        if year <= 0:
            print("Please enter only a positive year.")
        else:
            break 
    except ValueError:
        print("Strings are not allowed for Input")


if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
    print(f"{year} is a leap year.")
else:
    print(f"{year} is not a leap year.")



# Activity 2
rows = int(input("Enter the number of rows: "))
cols = int(input("Enter the number of columns: "))

for i in range(rows):
    for j in range(cols):
        print("*", end=" ")
    print() 
